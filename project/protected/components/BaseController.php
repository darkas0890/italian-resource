<?php

class BaseController extends CController 
{
    
    public function init()
    {
        parent::init();
    }

    protected function beforeAction($action)
    {
        return true;
    }
}
