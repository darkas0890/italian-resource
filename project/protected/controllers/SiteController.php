<?php

class SiteController extends Controller
{

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
            $this->render('index');
	}
        
        public function actionLang()
        {
            $lang = Yii::app()->request->getParam('lang', 'en');
            $_SESSION['lang'] = $lang;
            $user = User::model()->findByPk(Yii::app()->user->user->id);
            $user->language = $lang;
            $user->save(false);
            $back = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : array('/');
            $this->redirect($back);
        }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}