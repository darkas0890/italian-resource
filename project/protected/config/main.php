<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
session_start();
$lang = !empty($_SESSION['lang']) ? $_SESSION['lang'] : 'en';

return array(
    //'sourceLanguage' => 'en',
    'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'language' => $lang,
    'name' => 'My Project',
    
    'preload' => array('log'),
    
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        ),
    
    'components'=>array(
        'user'=>array(
            'allowAutoLogin'=>true,
            'class' => 'WebUser',
            ),
        
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName' => false,
            'caseSensitive' => false,  
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                '/' => 'site/index',
                ),
            ),

        'db'=>require(dirname(__FILE__).'/config_db.php'),

        'errorHandler'=>array(
            'errorAction'=>YII_DEBUG ? null : 'site/error',
            ),
        
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                            ),
                ),
            ),
        ),
    'params'=>array(
        'adminEmail'=>'webmaster@example.com',
        ),
    
    'modules'=>array(
        
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'142536',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
            ),
        ),
    );
