<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon-home.png" rel="icon" type="image/x-icon" />
        <?php
        Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/style.css' . Controller::ANTICACHE);
        Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/bootstrap/bootstrap.css' . Controller::ANTICACHE);
        Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/bootstrap/bootstrap-theme.css' . Controller::ANTICACHE);
        Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/select2/select2.min.css' . Controller::ANTICACHE);
        
        Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_HEAD;
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/bootstrap/bootstrap.min.js' . Controller::ANTICACHE);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/meiomask/meiomask.min.js' . Controller::ANTICACHE);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/bootstrap/bootstrap-timepicker.js' . Controller::ANTICACHE);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/main.js' . Controller::ANTICACHE);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/select2/select2.full.min.js' . Controller::ANTICACHE);
        ?>


	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
    <div class="wrap container-fluid">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    
                    <div class="navbar-menu float-left">
                        <?php

                $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions' => array('id' => 'accordion'),
                    'activateParents' => true,
                    'encodeLabel' => false,
                    'items'=>array(
                        array(
                            'label' => 'Home', 
                            'url' => array('/site/index')
                            ),
                        array(
                            'label' => 'Contact', 
                            'url' => array('/site/contact'),
                            'visible' => 1,
                            ),
                        array(
                            'label' => 'Product', 
                            'url' => array('/site/index')
                            ),
                        array(
                            'label' => 'Contact', 
                            'url' => array('/site/index'),
                            'itemOptions' => array('class' => 'right-menu')
                            ),
			),
                    )
                        );?>
                    </div>
                    <form class="navbar-form navbar-search" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="main row bg-gray-dark">
            <div class="menu bg-gray-dark">
                
            </div>
            <div class="content container">
                <div class="row content-float">
                    <div class="col-xs-12">
                        <?php if (isset($this->breadcrumbs)): ?>
                        <div class="col-xs-12">
                            <?php
                            $this->widget('zii.widgets.CBreadcrumbs', array(
                                'links' => $this->breadcrumbs,
                                ));
                            ?>
                        </div>
                            <?php endif ?>
                                <?php echo $content; ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo 'Powered by <a href="/">Pharmacy</a>'; ?>
            </div>
        </div>
    </div>
</body>
</html>
