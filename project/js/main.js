jQuery(document).ready(function () {
    var activeIndex = $('#accordion>li.dropdown').index($('li.dropdown.active'));
    $("#accordion").accordion({
        header: '> li.dropdown > :first-child,> :not(li):even',
        heightStyle: 'content',
        active: activeIndex == -1 ? false : activeIndex,
        collapsible: true
    });
});
